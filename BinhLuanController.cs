﻿using _1461270_web.Models.BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _1461270_web.Controllers
{
    public class BinhLuanController : Controller
    {
        //
        // GET: /BinhLuan/
        
        //[Authorize]

        public ActionResult Create(int MaSP, string NoiDung)
        {
            BinhLuanBus.Them(MaSP, NoiDung);
            return RedirectToAction("Details", "SanPham", new { id = MaSP});
        }

        public ActionResult Index(int MaSP)
        {
            ViewBag.MaSP = MaSP;
            return View(BinhLuanBus.DanhSach(MaSP));
        }
    }
}
