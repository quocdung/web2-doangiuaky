﻿using _1461270_web.Models;
using _1461270_web.Models.BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _1461270_web.Controllers
{
    public class SanPhamController : Controller
    {
        //
        // GET: /SanPham/

        public ActionResult Index()
        {
            var dsSanPham = SanPhamBus.DanhSach();
            return View(dsSanPham);
        }

        //
        // GET: /SanPham/Details/5

        public ActionResult Details(int id)
        {
            var sp = SanPhamBus.ChiTiet(id);

            return View(SanPhamBus.ChiTiet(id));
        }

        //
        // GET: /SanPham/Create

        public ActionResult Create()
        {
            
            return View();
        }

        //
        // POST: /SanPham/Create

        [HttpPost]
        public ActionResult Create(DreamerShopConnection.SanPham sp)
        {
            //try
            //{
            // TODO: Add insert logic here
            if (HttpContext.Request.Files.Count > 0)
            {
                var hpf = HttpContext.Request.Files[0];
                if (hpf.ContentLength > 0)
                {
                    string fileName = Guid.NewGuid().ToString();

                    string fullPathWithFileName = "/images/products" + fileName + ".jpg";
                    hpf.SaveAs(Server.MapPath(fullPathWithFileName));
                    sp.HinhAnh = fullPathWithFileName;
                }
            }
            SanPhamBus.ThemSanPham(sp);
            return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}
        }

        //
        // GET: /SanPham/Edit/5

        public ActionResult Edit(int id)
        {
            return View(SanPhamBus.ChiTiet(id));
        }

        //
        // POST: /SanPham/Edit/5

        [HttpPost]
        public ActionResult Edit(int id,DreamerShopConnection.SanPham sp)
        {
            //try
            //{
                // TODO: Add update logic here
                SanPhamBus.SuaSanPham(id, sp);
                return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}
        }

        //
        // GET: /SanPham/Delete/5

        public ActionResult Delete(int id)
        {
            return View(SanPhamBus.ChiTiet(id));
        }

        //
        // POST: /SanPham/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, DreamerShopConnection.SanPham sp)
        {
            try
            {
                // TODO: Add delete logic here
                SanPhamBus.XoaSanPham(id, sp);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
