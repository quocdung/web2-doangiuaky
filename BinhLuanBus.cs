﻿using DreamerShopConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _1461270_web.Models.BUS
{
    public class BinhLuanBus
    {
        public static void Them(int MaSP, string NoiDung)
        {
            using (var db = new DreamerShopConnectionDB())
            {
                BinhLuan binhluan = new BinhLuan();
                binhluan.MaSP = MaSP;
                //binhluan.MaTaiKhoan = MaTaiKhoan;
                binhluan.NoiDung = binhluan.NoiDung;
                db.Execute("INSERT INTO [dbo].[BinhLuan]([MaSP], [NoiDung]) VALUES (@0, @1)", MaSP, NoiDung);
            }
        }

        public static IEnumerable<BinhLuan> DanhSach(int MaSP)
        {
            using (var db = new DreamerShopConnectionDB())
            {
                return db.Query<BinhLuan>("select * from BinhLuan where MaSP = @0", MaSP);
            }
        }
    }
}