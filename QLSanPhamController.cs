﻿using _1461270_web.Areas.Admin.Models.Bus;
using _1461270_web.Models.BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _1461270_web.Models.ViewModels;

namespace _1461270_web.Areas.Admin.Controllers
{
    public class QLSanPhamController : Controller
    {
        //
        // GET: /Admin/QLSanPham/

        public ActionResult Index()
        {
            return View(SanPhamAdminBus.DanhSach());
        }

        //
        // GET: /Admin/QLSanPham/Details/5

        public ActionResult Details(int id)
        {
            var sp = SanPhamBus.ChiTiet(id);
            var nsx = NhaSXBus.Get(sp.MaNSX);
            return View(new SanPhamViewModel() { SanPham = sp, NhaSX = nsx });
        }

        //
        // GET: /Admin/QLSanPham/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Admin/QLSanPham/Create

        [HttpPost]
        public ActionResult Create(DreamerShopConnection.SanPham sp)
        {
            if (HttpContext.Request.Files.Count > 0)
            {
                var hpf = HttpContext.Request.Files[0];
                if (hpf.ContentLength > 0)
                {
                    string fileName = Guid.NewGuid().ToString();

                    string fullPathWithFileName = "/images/products" + fileName + ".jpg";
                    hpf.SaveAs(Server.MapPath(fullPathWithFileName));
                    sp.HinhAnh = fullPathWithFileName;
                }
            }
            SanPhamBus.ThemSanPham(sp);
            return RedirectToAction("Index");
        }

        //
        // GET: /Admin/QLSanPham/Edit/5

        public ActionResult Edit(int id)
        {
            return View(SanPhamAdminBus.ChiTiet(id));
        }

        //
        // POST: /Admin/QLSanPham/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, DreamerShopConnection.SanPham sp)
        {
            try
            {
                // TODO: Add update logic here
                SanPhamBus.SuaSanPham(id, sp);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/QLSanPham/Delete/5

        public ActionResult Delete(int id)
        {
            return View(SanPhamBus.ChiTiet(id));
        }

        //
        // POST: /Admin/QLSanPham/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, DreamerShopConnection.SanPham sp)
        {
            try
            {
                // TODO: Add delete logic here
                SanPhamBus.XoaSanPham(id, sp);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
