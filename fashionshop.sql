use master
go
if DB_ID('DreamerShop') is not null
	drop database DreamerShop
go
create database DreamerShop
go 
use DreamerShop
go 

--
-- Table structure for table `chitietdonhang`
--

CREATE TABLE ChiTietDonHang (
  MaDonHang int NOT NULL,
  MaTaiKhoan int NOT NULL,
  TenKH nvarchar(50),
  TenSanPham nvarchar(50),
  GiaSp int NOT NULL,
  SoLuongSP int NOT NULL,
  TongTien int NOT NULL,
  DiaChi nvarchar(50) NOT NULL,
  SDT int NOT NULL,
  constraint pk_ChiTietDonHang primary key (MaDonHang)
) 
-- --------------------------------------------------------

--
-- Table structure for table `dondathang`
--

CREATE TABLE DonDatHang (
  MaDonDatHang int NOT NULL,
  MaTaiKhoan int NOT NULL,
  MaSP int NOT NULL,
  SoLuongSP int NOT NULL,
  TongTien int NOT NULL,
  TenKH nvarchar(50)  NOT NULL,
  DiaChi varchar(50) NOT NULL,
  constraint pk_DonDatHang primary key (MaDonDatHang)
) 

-- --------------------------------------------------------

--
-- Table structure for table `loaisp`
--

CREATE TABLE LoaiSp (
	MaLoaiSP int NOT NULL,
	TenLoaiSP nvarchar(50) NOT NULL,
	constraint pk_LoaiSP primary key (MaLoaiSP)
)  ;

--
-- Dumping data for table `loaisp`
--

INSERT INTO LoaiSp (MaLoaiSP, TenLoaiSP) VALUES
(1, 'Loongtee, Layered'),
(2, 'Ão SÆ¡mi'),
(3, 'Quáº§n Skinny'),
(4, 'Quáº§n Short'),
(5, 'Ão Thun'),
(6, 'Äáº§m BabyDoll'),
(7, 'Äáº§m Dáº¡ Há»™i'),
(8, 'GiÃ y Sneaker'),
(9, 'GiÃ y Cao GÃ³t, BÃºp BÃª'),
(10, 'Hoodie, Sweater, Jacket');

-- --------------------------------------------------------

--
-- Table structure for table `loaitk`
--

CREATE TABLE LoaiTK  (
  MaLoaiTK int NOT NULL,
  LoaiTK nvarchar(10) NOT NULL,
  constraint pk_LoaiTK primary key (MaLoaiTK)
  
) 

--
-- Dumping data for table `loaitk`
--

INSERT INTO LoaiTK (MaLoaiTK, LoaiTK) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `nhasx`
--

CREATE TABLE NhaSX (
	MaNhaSX int NOT NULL,
	TenNhaSX nvarchar(50)  NOT NULL,
  constraint pk_NhaSX primary key (MaNhaSX)
	
)

--
-- Dumping data for table `nhasx`
--

INSERT INTO NhaSX (MaNhaSX, TenNhaSX) VALUES
(1, 'Zombie shop'),
(2, 'Bstore'),
(3, 'VIP'),
(4, 'Totoro '),
(5, 'BJ');

-- --------------------------------------------------------

--
-- Table structure for table `sanpham`
--

CREATE TABLE SanPham (
  MaSP int NOT NULL,
  TenSP nvarchar(50) NOT NULL,
  MoTa Text NOT NULL,
  XuatXu nvarchar(50)  NOT NULL,
  MaNSX int NOT NULL,
  GiaSP int NOT NULL,
  SoLuongBan int NOT NULL,
  MaLoaiSP int NOT NULL,
  SoLuongXem int NOT NULL,
  HinhAnh nvarchar(255) NOT NULL,
  constraint pk_SanPham primary key (MaSP)
  
) 


--
-- Dumping data for table `sanpham`
--

INSERT INTO SanPham (MaSP, TenSP, MoTa, XuatXu, MaNSX, GiaSP, SoLuongBan, MaLoaiSP, SoLuongXem, HinhAnh) VALUES
(1, 'Tee Ziper', 'Hai mÃ u: Äen tráº¯ng.\r\nSize s,m,l,xl,xxl.\r\n', 'ThaiLand', 1, 170000, 0, 1, 0, 'longtee6.jpg'),
(2, 'Geisha mentle', 'Hai mÃ u: Äen tráº¯ng.\r\nSize: s,m,l,xl,xxl.', 'ThaiLand', 1, 280000, 0, 2, 0, 'somi2.jpg'),
(3, 'Demi Shirt', 'MÃ u: Jean.\r\nSize:s, m ,l.\r\nStyle: Street.', 'Quáº£ng ChÃ¢u', 1, 280000, 0, 2, 0, 'somi4.jpg'),
(4, 'Alien Kimono', 'MÃ u: Äen. Size: s, m, l. Style: Japan Printest.', 'Quáº£ng ChÃ¢u', 1, 250000, 0, 2, 0, 'somi3.jpg'),
(5, 'Holy Demi Short', 'MÃ u: Jean Demi. Size: 30 Ä‘áº¿n 40. Style: Street', 'Quáº£ng ChÃ¢u', 1, 210000, 0, 4, 0, 'ss.jpg'),
(6, 'Stranger Jean', 'MÃ u: Jean. Size: 30 Ä‘áº¿n 40. Style: Street.', 'ThaiLand', 1, 260000, 0, 4, 0, 'short.jpg'),
(7, 'Short in grey', 'MÃ u: grey. Size: 30 Ä‘áº¿n 40. Style: Street.', 'Quáº£ng ChÃ¢u', 1, 170000, 0, 4, 0, 'short2.jpg'),
(8, 'Short basic', 'MÃ u: Jean. Size: 30 Ä‘áº¿n 40. Style: Street.', 'ThaiLand', 1, 170000, 0, 4, 0, 'short3.jpg'),
(9, 'short in black', 'MÃ u: Ä‘en. Size: 30 Ä‘áº¿n 40. Style: Street.', 'ThaiLand', 1, 170000, 0, 4, 0, 'short4.jpg'),
(10, 'Shortfloral', 'MÃ u: Jean, hoa, Ä‘en, xÃ¡m. Size: 30 Ä‘áº¿n 40. Style: Street.', 'ThaiLand', 1, 170000, 0, 4, 0, 'short5.jpg'),
(11, 'Destroy jean in Black', 'MÃ u: Ä‘en. Size: 30 Ä‘áº¿n 40. Style: Street.', 'ThaiLand', 1, 250000, 0, 3, 0, 'skinny1.jpg'),
(12, 'Ripped Pain', 'MÃ u: Jean. Size: 30 Ä‘áº¿n 40. Style: Street.', 'Korea', 1, 280000, 0, 3, 0, 'skinny22.jpg'),
(13, 'Straight Jean Whith Rips', 'MÃ u: Jean. Size: 30 Ä‘áº¿n 40. Style: Street.', 'Korea', 1, 280000, 0, 3, 0, 'skinny3.jpg'),
(14, 'Holy Jean', 'MÃ u: Jean. Size: 30 Ä‘áº¿n 40. Style: Street.', 'Korea', 1, 350000, 0, 3, 0, 'skinny4.jpg'),
(15, 'Destroy jean in white ', 'MÃ u: tráº¯ng. Size: 30 Ä‘áº¿n 40. Style: Street.', 'Korea', 1, 350000, 0, 3, 0, 'skinny5.jpg'),
(16, 'Hoodie Bemindelu', 'Hai mÃ u: Äen tráº¯ng. Size s,m,l,xl,xxl.', 'Taiwan', 1, 630000, 0, 10, 0, 'be.jpg'),
(17, 'Sexy lip xx', 'Hai mÃ u: Äen tráº¯ng. Size: s,m,l,xl,xxl.', 'Taiwan', 1, 530000, 0, 10, 0, 'lip.jpg'),
(18, 'Hoodie Badvide', 'MÃ u: Äen. Size: s,m,l,xl,xxl.	', 'Taiwan', 1, 470000, 0, 10, 0, 'bad.jpg'),
(19, 'Hoodie Chromeheart', 'MÃ u: Äen. Size: s,m,l,xl,xxl.', 'Taiwan', 1, 470000, 0, 10, 0, 'chro.jpg'),
(20, 'Sweater Suede', 'MÃ u: Äen, VÃ ng, Äá», Xanh rÃªu. Size: s,m,l,xl,xxl.', 'Taiwan', 1, 370000, 0, 10, 0, 'SWEATERSUEDE.jpg'),
(21, 'Magic Dress', 'MÃ u: Äen. Size: S, M. Style: Lady.', 'Korea', 2, 510000, 0, 7, 0, 'magic.jpg'),
(22, 'Blooming Dress', 'MÃ u: Äá». Size: S, M. Style: Lady.	', 'Quáº£ng ChÃ¢u', 2, 650000, 0, 7, 0, 'blooming.jpg'),
(23, 'Hello Darling', 'MÃ u: Tráº¯ng. Size: S, M. Style: Lace.', 'Quáº£ng ChÃ¢u', 2, 520000, 0, 7, 0, 'darling.jpg'),
(24, 'Sheer Love', 'MÃ u: Tráº¯ng. Size: S. M. Style: Lace.', 'Quáº£ng ChÃ¢u', 2, 480000, 0, 7, 0, 'sheer.jpg'),
(25, 'Kissme Dress', 'MÃ u: Tráº¯ng. Size: S. M. Style: Lace.', 'Korea', 2, 570000, 0, 7, 0, 'kissme.jpg'),
(26, 'Secret Charm', 'MÃ u: Há»“ng, Tráº¯ng, Äen. Size: free dÆ°á»›i 50kg. Style: Princess.', 'Korea', 2, 850000, 0, 7, 0, 'charm.jpg'),
(27, 'BW Lace ', 'MÃ u: Äen. Size: freesize dÆ°á»›i 52kg Style: Lace.', 'Quáº£ng ChÃ¢u', 2, 180000, 0, 6, 0, 'bwlace.jpg'),
(28, 'Shy Girl ', 'MÃ u: Tráº¯ng. Size: free dÆ°á»›i 50kg. Style: Princess.', 'Quáº£ng ChÃ¢u', 2, 370000, 0, 6, 0, 'princess.jpg'),
(29, 'Pink Girl ', 'MÃ u: Há»“ng. Size: free dÆ°á»›i 50kg. Style: Lace.', 'Quáº£ng ChÃ¢u', 2, 300000, 0, 6, 0, 'pinku.jpg'),
(30, 'Kaki Cute ', 'MÃ u: Äá», Äen. Size: free dÆ°á»›i 50kg. Style: lace.', 'Quáº£ng ChÃ¢u', 2, 200000, 0, 6, 0, 'kaki.jpg'),
(31, 'Luna Dress', 'MÃ u: Ä‘en. Size: S, M. Style: Lace.', 'Quáº£ng ChÃ¢u', 2, 350000, 0, 6, 0, 'luna.jpg'),
(32, 'Longtee Duck ', 'MÃ u: Äen tráº¯ng. Size: Free. Style: Street.', 'Taiwan', 3, 210000, 0, 1, 0, 'longtee1.jpg'),
(33, 'Longtee GeiSha', 'MÃ u: Äen tráº¯ng. Size: Free. Style: Street.', 'Taiwan', 3, 250000, 0, 1, 0, 'longtee2.jpg'),
(34, 'Longtee Break ', 'MÃ u: Äen tráº¯ng. Size: Free. Style: Street.', 'Taiwan', 3, 150000, 0, 1, 0, 'longtee3.jpg'),
(35, 'Tee Gray White ', 'MÃ u: sá»c tráº¯ng xÃ¡m. Size: Free. Style: Street.', 'Taiwan', 3, 150000, 0, 1, 0, 'longtee4.jpg'),
(36, 'GCDS Sweater ', 'MÃ u: RÃªu. Size: free. Style: street.', 'Taiwan', 3, 270000, 0, 10, 0, 'gc.jpg'),
(37, 'Converse Classic ', 'MÃ u: Full báº£ng MÃ u All Star Size: 36 Ä‘áº¿n 43. Style: Street.', 'Quáº£ng ChÃ¢u', 3, 210000, 0, 8, 0, 'cv1.jpg'),
(38, 'converse chuck 2 cá»• tháº¥p ', 'MÃ u: Full báº£ng MÃ u All Star Size: 36 Ä‘áº¿n 43. Style: Street.', 'Quáº£ng ChÃ¢u', 3, 250000, 0, 8, 0, 'cv2t.jpg'),
(39, 'Converse Chuck 2 cá»• cao', 'MÃ u: Full báº£ng MÃ u All Star Size: 36 Ä‘áº¿n 43. Style: Street.', 'Quáº£ng ChÃ¢u', 3, 300000, 0, 8, 0, 'cv2c.jpg'),
(40, 'Super Star', 'MÃ u: tráº¯ng. Size: 36 Ä‘áº¿n 43. Style: street.', 'Quáº£ng ChÃ¢u', 3, 350000, 0, 8, 0, 'superstar.jpg'),
(41, 'Floral Shirt ', 'MÃ u: hoa. Size: free. Style: Street.', 'Taiwan', 3, 210000, 0, 2, 0, 'ss.jpg'),
(42, 'Nike Air Max ', 'MÃ u: Tráº¯ng, Äen. Size: 36 Ä‘áº¿n 43. Style: Street.', 'Quáº£ng ChÃ¢u', 3, 470000, 0, 8, 0, 'nike.jpg'),
(43, 'Egg Eat Me ', 'MÃ u: Há»“ng. Size:s, m ,l. Style: baby.', 'Quáº£ng ChÃ¢u', 4, 120000, 0, 5, 0, 'thun1.jpg'),
(44, 'Pet Mickey ', 'MÃ u: Xanh lÃ¡. Size:s, m ,l. Style: baby.', 'Quáº£ng ChÃ¢u', 4, 120000, 0, 5, 0, 'thun2.jpg'),
(45, 'Ketchup Pink ', 'MÃ u: há»“ng. Size:s, m ,l. Style: baby.', 'Quáº£ng ChÃ¢u', 4, 120000, 0, 5, 0, 'thun3.jpg'),
(46, 'Wander Lust ', 'MÃ u: tráº¯ng. Size:s, m ,l. Style: baby.', 'Quáº£ng ChÃ¢u', 4, 120000, 0, 5, 0, 'thun4.jpg'),
(47, 'Water Melon ', 'MÃ u: Ä‘á». Size:s, m ,l. Style: baby.', 'Quáº£ng ChÃ¢u', 4, 120000, 0, 5, 0, 'thun5.jpg'),
(48, 'Rabit Line Friend', 'MÃ u: tráº¯ng, xÃ¡m. Size:s, m ,l. Style: baby.', 'Quáº£ng ChÃ¢u', 4, 120000, 0, 5, 0, 'thun6.jpg'),
(49, 'Zara Basic ', 'MÃ u: Ä‘en. Size: 36- 39. Style: lady.', 'US', 5, 510000, 0, 9, 0, 'zara.jpg'),
(50, 'Zara twinkle ', 'MÃ u: Ä‘en. Size: 36- 39. Style: lady.	', 'US', 5, 650000, 0, 9, 0, 'nt.jpg'),
(51, 'Sweet lady', 'MÃ u: Ä‘en. Size: 36- 39.Style: lady.', 'US', 5, 520000, 0, 9, 0, 'nude.jpg'),
(52, 'Christian Louboutin', 'MÃ u: Ä‘en Size: 36- 39. Style: lady	', 'US', 5, 1480000, 0, 9, 0, 'lt.jpg'),
(53, 'Aqua Baby Louboutin', 'MÃ u: Ä‘en. Size: 36- 39. Style: lady.', 'US', 5, 1570000, 0, 9, 0, 'lou.jpg'),
(54, 'Amber Dress ', 'MÃ u: Äen. Size: free. Style: lace.', 'Quáº£ng ChÃ¢u', 5, 210000, 0, 6, 0, 'amber.jpg'),
(55, 'Daisy Dress', 'MÃ u: Äen. Size: free. Style: lace.', 'Quáº£ng ChÃ¢u', 5, 250000, 0, 6, 0, 'daisy.jpg'),
(56, 'Kiko Dress ', 'MÃ u: tráº¯ng. Size: free. Style: lace.', 'Quáº£ng ChÃ¢u', 5, 330000, 0, 6, 0, 'kiko.jpg'),
(57, 'buoi', 'buoi', 'vietnam', 2, 999999999, 0, 3, 0, '4-den-Artemis1-470x320.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `taikhoan`
--

CREATE TABLE TaiKhoan (
  MaTaiKhoan int NOT NULL,
  TenTK nvarchar(50) NOT NULL,
  MatKhau nvarchar(50) NOT NULL,
  TenHienThi nvarchar(50) NOT NULL,
  NgaySinh date NOT NULL,
  MaLoaiTK int NOT NULL,
  DiaChi varchar(50)  NOT NULL,
  SDT int NOT NULL,
  constraint pk_TaiKhoan primary key (MaTaiKhoan)
  
) 

--
-- Dumping data for table `taikhoan`
--

INSERT INTO Taikhoan (MaTaiKhoan, TenTK, MatKhau, TenHienThi, NgaySinh, MaLoaiTK, DiaChi, SDT) VALUES
(1, 'Admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', '2016-12-01', 1, '123 phan van tri binh thanh tphcm', 1652550619),
(2, 'thanh hang', 'e10adc3949ba59abbe56e057f20f883e', 'LÃª Thanh Háº±ng', '2016-05-29', 2, '111 nguyen kim, go vap', 1666353501),
(3, 'dung', 'e10adc3949ba59abbe56e057f20f883e', 'Quá»‘c DÅ©ng', '2016-08-22', 2, '123 qlo 1A thu duc', 1639335268);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chitietdonhang`
