﻿using DreamerShopConnection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _1461270_web.Areas.Admin.Models.Bus
{
    public class SanPhamAdminBus
    {
        public static IEnumerable<DreamerShopConnection.SanPham> DanhSach()
        {
            var db = new DreamerShopConnectionDB();
            return db.Query<DreamerShopConnection.SanPham>("select * from SanPham");
        }

        public static Page<DreamerShopConnection.SanPham> DanhSach(int pageNumber, int itemsPerPage)
        {
            var db = new DreamerShopConnectionDB();
            return db.Page<DreamerShopConnection.SanPham>(pageNumber, itemsPerPage, "select * from SanPham");
        }

        public static SanPham ChiTiet(int id)
        {
            var db = new DreamerShopConnectionDB();
            return db.SingleOrDefault<SanPham>("select * from SanPham where MaSP = @0", id);
        }

        public static void ThemSanPham(DreamerShopConnection.SanPham sp)
        {
            var db = new DreamerShopConnectionDB();
            db.Insert(sp);
        }

        public static void XoaSanPham(int id, DreamerShopConnection.SanPham sp)
        {
            var db = new DreamerShopConnectionDB();
            db.Delete<SanPham>("where MaSP = @0", id);
        }

        public static void SuaSanPham(int id, DreamerShopConnection.SanPham sp)
        {
            var db = new DreamerShopConnectionDB();
            db.Update<DreamerShopConnection.SanPham>("SET TenSP = @0, MoTa = @1, XuatXu = @2, MaNSX = @3, GiaSP = @4, SoLuongBan = @5, MaLoaiSP = @6, HinhAnh = @7 where MaSP = @8", sp.TenSP, sp.MoTa, sp.XuatXu, sp.MaNSX, sp.GiaSP, sp.SoLuongBan, sp.MaLoaiSP, sp.HinhAnh, id);
        }
    }
}